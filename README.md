# LLM Provider Service

## Overview

Efforts are currently underway to integrate AI (specifically Large Language
Models, or LLMs) into the Drupal ecosystem, as evidenced by initiatives such
as the [OpenAI / ChatGPT Integration](https://www.drupal.org/project/openai).
However, there is a notable absence
of an LLM abstraction layer that would allow users to leverage the existing
functionality across various LLMs seamlessly.

This module is designed for developers, aiming to provide a unified interface
for accessing LLMs within Drupal. It introduces an LLM Provider Interface,
registers enabled LLM services, and offers a composite service encompassing
all enabled LLMs along with their listings.

## Usage

To use the LLM Provider Service module, enable the module as you would
with any other Drupal module.

Upon the availability of any compatible LLM Provider, the
LLM Chat Example submodule enables simple chat interactions with
the accessible LLMs. Users can freely select from the enabled LLMs.

## Requirements

- Drupal 9 or 10
- LLM Chat Example submodule requires at least one LLM Provider to be enabled.
  At the present time, [HuggingFace](https://www.drupal.org/project/huggingface)
  and [LM Studio](https://www.drupal.org/project/lmstudio) are available.

## Installation

1. Install as you would normally install a contributed Drupal module. Visit:
   [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
   for further information.
2. Enable the module through the Drupal administration interface or via Drush
   (`drush en llm_provider`).

## Support & Maintenance

For any issues or feature requests, please use the
[issue tracker](https://www.drupal.org/project/issues/llm_provider) on
the project page.

## Maintainer

Michal Gow (seogow) - https://www.drupal.org/u/seogow
