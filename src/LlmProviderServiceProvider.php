<?php

namespace Drupal\llm_provider;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Find and add all the service providers tagged with llm_provider.provider.
 */
class LlmProviderServiceProvider extends ServiceProviderBase {

  /**
   * Registers services to the container.
   *
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   *   The Container Builder.
   */
  public function alter(ContainerBuilder $container): void {
    if ($container->has('llm_provider.manager')) {
      $taggedServices = $container->findTaggedServiceIds('llm_provider.provider');
      $definition = $container->getDefinition('llm_provider.manager');
      foreach ($taggedServices as $id => $tags) {
        $definition->addMethodCall('addProvider', [new Reference($id), $tags[0]['id']]);
      }
    }
  }

}
