<?php

namespace Drupal\llm_provider_chat\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\llm_provider\Enum\Bundles;
use Drupal\llm_provider\Service\LlmProviderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for the LM Studio endpoint.
 */
class LLMProviderChatForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'llm_provider_chat_form';
  }

  /**
   * The LLM Service Manager.
   *
   * @var \Drupal\llm_provider\Service\LlmProviderManager|null
   */
  protected ?LlmProviderManager $llmServiceManager = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): LLMProviderChatForm {
    $instance = parent::create($container);
    $instance->llmServiceManager = $container->get('llm_provider.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $models = $this->llmServiceManager->getModels([Bundles::Chat->name]);
    $options = $this->buildModelOptions($models);
    $default_value = !empty($options) ? array_key_first($options) : NULL;
    $form['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#options' => $options,
      '#default_value' => $default_value,
      '#description' => $this->t('Select LLM API.'),
      '#ajax' => [
        'callback' => '::updateDependentField',
        'wrapper' => 'temperature-wrapper',
        'event' => 'change',
      ],
    ];

    $form['temperature'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Set temperature'),
      '#required' => TRUE,
      '#default_value' => !empty($options) ? $this->initialTemperature($options) : NULL,
      '#prefix' => '<div id="temperature-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ask LLM'),
      '#rows' => 1,
      '#description' => $this->t('Enter your query here.'),
      '#required' => TRUE,
    ];

    $form['response'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Response from OpenAI'),
      '#attributes' =>
        [
          'readonly' => 'readonly',
        ],
      '#prefix' => '<div id="llm-response">',
      '#suffix' => '</div>',
      '#description' => $this->t('The response from LLM.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::getResponse',
        'wrapper' => 'llm-response',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Render the LLM response.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The modified form element.
   */
  public function getResponse(array &$form, FormStateInterface $form_state): array {
    $storage = $form_state->getStorage();
    $last_response = end($storage['messages']);
    $form['response']['#value'] = trim($last_response['content']);
    return $form['response'];
  }

  /**
   * Submit the request.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $text = $form_state->getValue('text');
    $model_values = json_decode($form_state->getValue('model'), TRUE);
    $configuration['temperature'] = (float) $form_state->getValue('temperature');
    $storage = $form_state->getStorage();

    if (!empty($storage['messages'])) {
      $messages = $storage['messages'];
      $messages[] = ['role' => 'user', 'content' => trim($text)];
    }
    else {
      $messages = [
        ['role' => 'system', 'content' => 'You are polite and thoughtful assistant.'],
        ['role' => 'user', 'content' => trim($text)],
      ];
    }

    if ($model_values) {
      $responseContent = $this->llmServiceManager->getResponse($model_values['provider'], $model_values['model'], $messages, [], $configuration);
    }
    else {
      $responseContent = $this->t('No suitable LLM provider found.');
    }

    $messages[] = ['role' => 'assistant', 'content' => $responseContent];
    $form_state->setStorage(['messages' => $messages]);
    $form_state->setRebuild();
  }

  /**
   * Build options for Select.
   *
   * @param array $models
   *   Array of available models.
   *
   * @return array
   *   Array of select options.
   */
  private function buildModelOptions(array $models): array {
    $options = [];
    foreach ($models as $bundle => $providers) {
      foreach ($providers as $provider => $implementations) {
        foreach ($implementations as $name => $ids) {
          $groupLabel = "{$bundle} - {$provider}";
          $optionLabel = "{$name}";
          $key = json_encode($ids);
          $options[$groupLabel][$key] = $optionLabel;
        }
      }
    }

    return $options;
  }

  /**
   * Get initial value of temperature.
   *
   * @param array $options
   *   Options for Model select.
   *
   * @return float
   *   Value of default model temperature.
   */
  private function initialTemperature(array $options): float {
    $selected_value = json_decode(array_key_first($options[array_key_first($options)]), TRUE);
    $values = $this->llmServiceManager->getDefaultConfigurationValues($selected_value['provider'], $selected_value['model']);
    return (float) $values['temperature'];
  }

  /**
   * Update temperature field.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Return updated temperature value.
   */
  public function updateDependentField(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $selected_value = json_decode($form_state->getValue('model'), TRUE);
    $values = $this->llmServiceManager->getDefaultConfigurationValues($selected_value['provider'], $selected_value['model']);
    $response->addCommand(new InvokeCommand('#edit-temperature', 'val', [$values['temperature']]));
    return $response;
  }

}
